package com.flipkart.kafkautils.core;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.google.common.io.Resources;

public class KafkaConsumerService {
	Properties properties = new Properties();
	public KafkaConsumer<String, String>  getKafkConsumer(){
	
		
		 KafkaConsumer<String, String> consumer = null;
	 
		

			try {
				InputStream props = Resources.getResource("consumer.props").openStream();

				properties.load(props);
				 consumer = new KafkaConsumer<String, String>(properties);

			} catch (Exception e) {
				// TODO: handle exception
			}
			return consumer;
		

	}

	public String receive() {

		KafkaConsumer<String, String> consumer =getKafkConsumer();
		consumer.subscribe(Arrays.asList(properties.getProperty("topic")));

		String     msg = "";
		while (true) {
			ConsumerRecords<String, String> consumerRecords = consumer.poll(100);
			for (ConsumerRecord<String, String> consumerRecord : consumerRecords) {
				msg += consumerRecord.value();
			}
			consumer.close();
			return msg;
		}
	}

}
