package com.flipkart.kafkautils.core;

import java.io.InputStream;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.google.common.io.Resources;

public class KafkaProducerService {
	//private static KafkaProducer<String, String> producer;
	private static Properties properties = new Properties();

	public void sendMessage(String msg) {

		KafkaProducer<String, String> producer = getKafkaProducer();
		ProducerRecord<String, String> record = new ProducerRecord<String, String>(properties.getProperty("topic"),
				msg);
		producer.send(record);

		producer.close();

	}

	private KafkaProducer<String, String> getKafkaProducer() {
		KafkaProducer<String, String> producer = null; 
		try {
			InputStream props = Resources.getResource("producer.props").openStream();

			properties.load(props);
			producer = new KafkaProducer<String, String>(properties);

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return producer;

	}
}
